package kbc.trafficlight

import android.app.Application
import android.util.Log
import androidx.compose.animation.core.animateDecay
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin


class TrafficLightApplication : Application() {
    override fun onCreate() {
        super.onCreate()

        startKoin() {
            androidLogger()
            androidContext(this@TrafficLightApplication)
            modules(trafficModule)
        }
    }
}