package kbc.trafficlight.domain.model

data class Car(val model: String)