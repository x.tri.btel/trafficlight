package kbc.trafficlight.domain.model

enum class TrafficLight {
    RED,
    ORANGE,
    GREEN,
}