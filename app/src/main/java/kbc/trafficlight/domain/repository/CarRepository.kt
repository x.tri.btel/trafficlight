package kbc.trafficlight.domain.repository

import kbc.trafficlight.domain.model.Car

interface CarRepository {
    fun getCar(): Car
    fun setCar(car: Car)
}