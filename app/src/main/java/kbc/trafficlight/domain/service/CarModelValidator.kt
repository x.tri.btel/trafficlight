package kbc.trafficlight.domain.service

class CarModelValidator : Validator {
    override val errorMessage: String
        get() = "Minimum Length is 3"

    override  fun validateInput(input: String?) = input?.let { it.length >= 3 } ?: false

}

interface Validator {
    val errorMessage: String

    fun validateInput(input: String?): Boolean
}