package kbc.trafficlight.domain.service

import kbc.trafficlight.domain.model.TrafficLight
import kbc.trafficlight.ui.navigation.NavRoute
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.flow

class TrafficLightService {

    suspend fun activeTrafficLight() = flow<TrafficLight> {
        val initialLight = TrafficLight.RED
        emit(initialLight)

        while (true) {
            delay(4000)
            emit(TrafficLight.ORANGE)
            delay(1000)
            emit(TrafficLight.GREEN)
            delay(4000)
            emit(TrafficLight.RED)
        }
    }
}