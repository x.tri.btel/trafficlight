package kbc.trafficlight.ui.carmodel

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Close
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedButton
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import kbc.trafficlight.R
import org.koin.androidx.compose.koinViewModel

@Preview
@Composable
fun CarModelScreen(
    viewModel: CarModelViewModel = koinViewModel(),
    navigateToTrafficLight: () -> Unit = {}
) {
    val text = rememberSaveable() { mutableStateOf("") }
    val isInputValid by viewModel.isCarModelValid.collectAsState(false)
    val onTextChanged: (String) -> Unit = {
        viewModel.onCarModelEntered(it)
        text.value = it
    }
    Column(
        Modifier
            .padding(18.dp)
            .fillMaxWidth()
            .fillMaxHeight(),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Spacer(modifier = Modifier.height(32.dp))
        CarModelTextView(text.value, isInputValid, onTextChanged)
        Spacer(modifier = Modifier.height(8.dp))
        CarModelButton(isInputValid, viewModel::onContinueClicked)
    }

    LaunchedEffect(Unit) {
        viewModel.continueEvent.collect {
            navigateToTrafficLight.invoke()
        }
    }
}

@Preview
@Composable
fun CarModelTextView(
    text: String = "",
    isInputValid: Boolean = false,
    onTextChanged: (String) -> Unit = {}
) {
    OutlinedTextField(
        value = text,
        onValueChange = { onTextChanged.invoke(it) },

        label = { Text(stringResource(id = R.string.car_model_label)) },
        isError = !isInputValid,
        trailingIcon = {
            if (!isInputValid) {
                Icon(
                    imageVector = Icons.Filled.Close,
                    contentDescription = stringResource(id = androidx.compose.ui.R.string.default_error_message),
                    tint = MaterialTheme.colorScheme.error
                )
            }
        }
    )
}

@Preview
@Composable
fun CarModelButton(
    isInputValid: Boolean = false, onContinueClicked: () -> Unit = {}
) {
    OutlinedButton(onClick = { onContinueClicked.invoke() }, enabled = isInputValid) {
        Text(text = stringResource(id = R.string.start_driving))
    }
}
