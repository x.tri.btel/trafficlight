package kbc.trafficlight.ui.carmodel

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kbc.trafficlight.domain.model.Car
import kbc.trafficlight.domain.repository.CarRepository
import kbc.trafficlight.domain.service.CarModelValidator
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.channels.ChannelResult
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.launch

class CarModelViewModel(
    private val handle: SavedStateHandle,
    private val validator: CarModelValidator,
    private val repository: CarRepository
) : ViewModel() {

    val _continueEvent = Channel<ContinueEvent>()
    val continueEvent = _continueEvent.receiveAsFlow()

    val carModelInput = handle.getStateFlow(KEY_CAR_MODEL, "")

    val isCarModelValid = combine(carModelInput) {
        validator.validateInput(carModelInput.value)
    }

    fun onCarModelEntered(carModel: String) {
        handle[KEY_CAR_MODEL] = carModel
    }

    fun onContinueClicked() {
        val car = Car(carModelInput.value)
        repository.setCar(car)
        viewModelScope.launch {
            _continueEvent.send(ContinueEvent(car))
        }
    }

    companion object {
        const val KEY_CAR_MODEL = "CAR_MODEL"

    }
}

data class ContinueEvent(
    val car: Car
)