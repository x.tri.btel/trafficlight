package kbc.trafficlight.ui.navigation

sealed class NavRoute(val path: String) {

    object CarModel: NavRoute("carModel")
    object TrafficLight: NavRoute("trafficLight")

    // build navigation path (for screen navigation)
    fun withArgs(vararg args: String): String {
        return buildString {
            append(path)
            args.forEach{ arg ->
                append("/$arg")
            }
        }
    }

    fun withArgsFormat(vararg args: String) : String {
        return buildString {
            append(path)
            args.forEach{ arg ->
                append("/{$arg}")
            }
        }
    }
}
