package kbc.trafficlight.ui.navigation

import androidx.compose.runtime.Composable
import androidx.navigation.NavController
import androidx.navigation.NavGraphBuilder
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import kbc.trafficlight.ui.carmodel.CarModelScreen
import kbc.trafficlight.ui.traficlight.TrafficLight
import kbc.trafficlight.ui.traficlight.TrafficLightScreen

@Composable
fun NavGraph(navController: NavHostController) {
    NavHost(
        navController = navController,
        startDestination = NavRoute.CarModel.path
    ) {
        addCarModelScreen(navController, this)
        addTrafficLightScreen(navController, this)
    }
}

private fun addCarModelScreen(
    navController: NavHostController,
    navGraphBuilder: NavGraphBuilder
) {
    navGraphBuilder.composable(route = NavRoute.CarModel.path) {
        CarModelScreen(
            navigateToTrafficLight = { ->
                navController.navigate(NavRoute.TrafficLight.path)
            }
        )
    }
}

private fun addTrafficLightScreen(
    navController: NavController,
    navGraphBuilder: NavGraphBuilder
) {
    navGraphBuilder.composable(
        route = NavRoute.TrafficLight.path
    ) {
        TrafficLightScreen()
    }
}
