package kbc.trafficlight.ui.traficlight

import androidx.compose.animation.animateColorAsState
import androidx.compose.foundation.Canvas
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import kbc.trafficlight.domain.model.TrafficLight
import kbc.trafficlight.ui.theme.GreenActive
import kbc.trafficlight.ui.theme.GreenInactive
import kbc.trafficlight.ui.theme.OrangeActive
import kbc.trafficlight.ui.theme.OrangeInactive
import kbc.trafficlight.ui.theme.RedActive
import kbc.trafficlight.ui.theme.RedInactive
import org.koin.androidx.compose.koinViewModel


@Preview
@Composable
fun TrafficLightScreen(
    viewModel: TrafficLightViewModel = koinViewModel()
) {
    val modelText = viewModel.getCarModel()
    val currentTrafficLight by viewModel.activeTrafficLight.collectAsState()
    Column(
        Modifier
            .padding(16.dp)
            .fillMaxWidth()
            .fillMaxWidth(),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Spacer(modifier = Modifier.height(16.dp))
        CarModelTextView(modelText)
        Spacer(modifier = Modifier.height(8.dp))
        TrafficLight(currentTrafficLight)
    }
    LaunchedEffect(Unit) {
        viewModel.startTrafficLight()
    }
}

@Preview
@Composable
fun CarModelTextView(carModel: String = "Example Car") {
    Text(text = carModel, fontSize = 24.sp)
}

@Preview
@Composable
fun TrafficLight(activeTrafficLight: TrafficLight = TrafficLight.RED) {
    Column(
        modifier = Modifier
            .background(color = Color.DarkGray, shape = RoundedCornerShape(20.dp))
            .padding(32.dp)
            .width(32.dp)
            .height(160.dp),
        verticalArrangement = Arrangement.SpaceEvenly,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {

        val lightModifier = Modifier
            .fillMaxWidth(1f)
            .aspectRatio(1f)
            .weight(1f)
            .padding(4.dp)

        val redLightColor: Color by animateColorAsState(
            if (activeTrafficLight == TrafficLight.RED) RedActive else RedInactive
        )
        val orangeLightColor: Color by animateColorAsState(
            if (activeTrafficLight == TrafficLight.ORANGE) OrangeActive else OrangeInactive
        )
        val greenLightColor: Color by animateColorAsState(
            if (activeTrafficLight == TrafficLight.GREEN) GreenActive else GreenInactive
        )

        Light(
            lightModifier,
            redLightColor
        )
        Light(
            lightModifier,
            orangeLightColor
        )
        Light(
            lightModifier,
            greenLightColor
        )
    }

}

@Composable
fun Light(modifier: Modifier, color: Color) {
    Canvas(modifier = modifier) {
        val center = Offset(size.width / 2, size.height / 2)
        drawCircle(color = color, radius = size.minDimension, center = center)
    }
}