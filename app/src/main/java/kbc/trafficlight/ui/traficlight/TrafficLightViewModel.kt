package kbc.trafficlight.ui.traficlight

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kbc.trafficlight.domain.model.TrafficLight
import kbc.trafficlight.domain.repository.CarRepository
import kbc.trafficlight.domain.service.TrafficLightService
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch

class TrafficLightViewModel(
    private val repository: CarRepository,
    private val trafficLightService: TrafficLightService
) : ViewModel() {

    private val _activeTrafficLight = MutableStateFlow<TrafficLight>(TrafficLight.RED)
    val activeTrafficLight = _activeTrafficLight

    fun startTrafficLight() {
        viewModelScope.launch {

            trafficLightService.activeTrafficLight().collect() {
                _activeTrafficLight.emit(it)
            }
        }
    }

    fun getCarModel() = repository.getCar().model
}