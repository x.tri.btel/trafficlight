package kbc.trafficlight

import kbc.trafficlight.data.repository.CarDataSource
import kbc.trafficlight.data.repository.InMemoryCarRepository
import kbc.trafficlight.domain.repository.CarRepository
import kbc.trafficlight.domain.service.CarModelValidator
import kbc.trafficlight.domain.service.TrafficLightService
import kbc.trafficlight.ui.carmodel.CarModelViewModel
import kbc.trafficlight.ui.traficlight.TrafficLightViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module


val trafficModule = module {

    viewModel {
        CarModelViewModel(get(), get(), get())
    }
    viewModel {
        TrafficLightViewModel(get(), get())
    }

    single { CarModelValidator() }
    single<CarRepository> { InMemoryCarRepository(get()) }
    single { CarDataSource() }
    single { TrafficLightService() }

}