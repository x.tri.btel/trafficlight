package kbc.trafficlight.data.repository

import kbc.trafficlight.domain.model.Car
import kbc.trafficlight.domain.repository.CarRepository

class InMemoryCarRepository(private val dataSource: CarDataSource) : CarRepository {
    override fun getCar() = dataSource.currentCar ?: Car("Unknown")

    override fun setCar(car: Car) {
        dataSource.currentCar = car
    }
}