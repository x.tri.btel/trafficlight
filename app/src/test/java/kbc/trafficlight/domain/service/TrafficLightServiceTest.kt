package kbc.trafficlight.domain.service

import kbc.trafficlight.domain.model.TrafficLight
import kotlinx.coroutines.flow.take
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Test


class TrafficLightServiceTest {
    lateinit var sut: TrafficLightService

    @Before
    fun setup() {
        sut = TrafficLightService()
    }

    @Test
    fun `test order of traffic lights colors`() = runTest {
        val list = sut.activeTrafficLight().take(3).toList()

        assert(list[0] == TrafficLight.RED)
        assert(list[1] == TrafficLight.ORANGE)
        assert(list[2] == TrafficLight.GREEN)

    }
}