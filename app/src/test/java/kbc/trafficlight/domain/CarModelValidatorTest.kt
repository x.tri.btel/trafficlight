package kbc.trafficlight.domain

import kbc.trafficlight.domain.service.CarModelValidator
import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Test


class CarModelValidatorTest {

    val SUT = CarModelValidator()

    @Test
    fun `should successfully validate Strings longer than 3 characters`() {
        assertTrue(SUT.validateInput("Toyota"))
        assertTrue(SUT.validateInput("Volvo"))
        assertTrue(SUT.validateInput("Suzuki"))
    }

    @Test
    fun `should fail to validate Strings less than 3 characters`() {
        assertFalse(SUT.validateInput("s5"))
        assertFalse(SUT.validateInput("s"))
        assertFalse(SUT.validateInput(""))
    }
}