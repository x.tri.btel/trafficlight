package kbc.trafficlight

import kbc.trafficlight.domain.model.Car
import kbc.trafficlight.domain.model.TrafficLight.*
import kotlinx.coroutines.flow.flowOf


fun someCar(carModel: String = "Mocked Car") = Car(carModel)

fun someTrafficLightFlow() = flowOf(RED, ORANGE,GREEN)