package kbc.trafficlight.ui.traficlight

import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.impl.annotations.MockK
import kbc.trafficlight.common.BaseViewModelTest
import kbc.trafficlight.domain.repository.CarRepository
import kbc.trafficlight.domain.service.TrafficLightService
import kbc.trafficlight.someTrafficLightFlow
import kotlinx.coroutines.flow.last
import kotlinx.coroutines.flow.take
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Test


class TrafficLightViewModelTest : BaseViewModelTest() {

    lateinit var sut: TrafficLightViewModel

    @MockK(relaxed = true)
    lateinit var trafficLightService: TrafficLightService

    @MockK(relaxed = true)
    lateinit var repository: CarRepository

    @Before
    fun setup() {
        MockKAnnotations.init(this)
        sut = TrafficLightViewModel(repository, trafficLightService)
    }

    @Test
    fun `should start emitting items after start is called`() = runTest {
        coEvery { trafficLightService.activeTrafficLight() } returns someTrafficLightFlow()

        sut.startTrafficLight()

        assert(sut.activeTrafficLight.take(1).last() == someTrafficLightFlow().last())

    }

}

