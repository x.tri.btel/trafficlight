package kbc.trafficlight.ui.carmodel

import androidx.lifecycle.SavedStateHandle
import app.cash.turbine.test
import io.mockk.MockKAnnotations
import io.mockk.impl.annotations.MockK
import kbc.trafficlight.common.BaseViewModelTest
import kbc.trafficlight.domain.repository.CarRepository
import kbc.trafficlight.domain.service.CarModelValidator
import kbc.trafficlight.ui.carmodel.CarModelViewModel.Companion.KEY_CAR_MODEL
import kotlinx.coroutines.flow.consumeAsFlow
import kotlinx.coroutines.test.runTest
import org.junit.Assert.*

import org.junit.Before
import org.junit.Test

class CarModelViewModelTest : BaseViewModelTest() {
    lateinit var sut: CarModelViewModel

    @MockK(relaxed = true)
    lateinit var handle: SavedStateHandle

    @MockK(relaxed = true)
    lateinit var validator: CarModelValidator

    @MockK(relaxed = true)
    lateinit var repository: CarRepository

    @Before
    fun setUp() {
        MockKAnnotations.init(this)
        handle = SavedStateHandle()
        sut = CarModelViewModel(handle, validator, repository)
    }

    @Test
    fun `should receive car model when onCarModelEntered is called`() {
        val carModel = "Ferrari"

        sut.onCarModelEntered(carModel)

        assertEquals(handle.get<String>(KEY_CAR_MODEL), carModel)
    }

    @Test
    fun `should receive continue event when onContinueClicked is called`() = runTest {
        val carModel = "Ferrari"
        sut.onCarModelEntered(carModel)

        sut.onContinueClicked()

        sut.continueEvent.test {
            assertEquals(this.awaitItem().car.model, carModel)
        }
    }
}