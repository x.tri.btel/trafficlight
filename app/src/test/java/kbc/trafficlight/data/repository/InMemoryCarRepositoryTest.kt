package kbc.trafficlight.data.repository

import io.mockk.MockKAnnotations
import io.mockk.every
import io.mockk.impl.annotations.MockK
import kbc.trafficlight.domain.repository.CarRepository
import kbc.trafficlight.domain.service.TrafficLightService
import org.junit.Before
import org.junit.Test
import io.mockk.verify
import kbc.trafficlight.someCar

class InMemoryCarRepositoryTest {

    lateinit var sut: InMemoryCarRepository

    @MockK(relaxed = true)
    private lateinit var dataSource: CarDataSource

    @Before
    fun setup() {
        MockKAnnotations.init(this)
        sut = InMemoryCarRepository(dataSource)
    }

    @Test
    fun `should store car in data source`() {
        val car = someCar()

        sut.setCar(car)

        verify { dataSource.currentCar = car }
    }

    @Test
    fun `should get car from data source`() {
        every { dataSource.currentCar } returns someCar()

        sut.getCar()

        verify(atLeast = 1) { dataSource.currentCar }
    }
}